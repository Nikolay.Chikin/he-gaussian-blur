# Gaussian blur algorithm with CKKS homomorphic encryption scheme

[![pipeline status](https://gitlab.unige.ch/Nikolay.Chikin/he-gaussian-blur/badges/main/pipeline.svg)](https://gitlab.unige.ch/Nikolay.Chikin/he-gaussian-blur/-/commits/main)

This project implements a Gaussian blur algorithm with CKKS homomorphic encryption scheme

![Comparison plot](doc/comparison_plot.png)

## Example results

See the latest successful CI pipeline job artifacts

## How to run

See [Dockerfile](Dockerfile) and [.gitlab-ci.yml](.gitlab-ci.yml)

## Options

```bash
python3 src/main.py --help
```

```text
usage: main.py [-h] img_path work_dir number_of_threads number_of_runs sigma kernel_size img_scale

positional arguments:
  img_path           Path to source image
  work_dir           Path to the directory where to write the results
  number_of_threads  Number of threads
  number_of_runs     For how many runs to calculate the average and standard deviation execution time
  sigma              Gaussian blur sigma
  kernel_size        Gaussian blur kernel radius. Kernel is a square with side of 2 * kernel_size + 1
  img_scale          Float <= 1, scales the source image

options:
  -h, --help         show this help message and exit
```

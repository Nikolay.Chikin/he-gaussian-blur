import os

import app
import config
import eva
import util


def main():
    args = util.parser.parse_args()

    eva.set_num_threads(args.number_of_threads)

    client = app.Client(args.img_path, args.work_dir, args.sigma, args.kernel_size, args.img_scale)
    server = app.Server(args.work_dir)

    os.makedirs(args.work_dir, exist_ok=True)

    util.save_args(args)
    util.save_cpu_info(f'{args.work_dir}/cpu_info.txt')
    util.measure_artefact_sizes(config.full_run(client, server), args.work_dir)
    util.measure_time(config.first_run(client, server), args.number_of_runs, f'{args.work_dir}/first_run_times.txt')
    util.measure_time(config.second_run(client, server), args.number_of_runs, f'{args.work_dir}/second_run_times.txt')
    util.save_rusage(f'{args.work_dir}/rusage.txt')

    os.system(f'rm {args.work_dir}/*.bin')

if __name__ == "__main__":
    main()

import eva


class Server:
    name = 'server'

    eva_fields = [
        'public_ctx',
        'compiled',
    ]

    def __init__(self, work_dir):
        self.work_dir = work_dir

    def load_inputs(self):
        self.enc_inputs = eva.load(f'{self.work_dir}/enc_inputs.bin')

    def load_state(self):
        for eva_field in self.eva_fields:
            setattr(self, eva_field, eva.load(f'{self.work_dir}/{eva_field}.bin'))

    def execute_program(self):
        self.enc_outputs = self.public_ctx.execute(self.compiled, self.enc_inputs)

    def save_outputs(self):
        eva.save(self.enc_outputs, f'{self.work_dir}/enc_outputs.bin')

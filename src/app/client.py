import os

import eva
import matplotlib.pyplot as plt
import numpy as np
import scipy
import skimage
from eva.ckks import CKKSCompiler
from eva.seal import generate_keys


class Client:
    name = 'client'
    channels = ['r', 'g', 'b']

    eva_fields = [
        'program',
        'compiled',
        'params',
        'signature',
        'public_ctx',
        'secret_ctx',
    ]

    compiler_config = {
        'warn_vec_size': 'false',
    }

    def __init__(self, img_path, work_dir, sigma, kernel_size, img_scale):
        self.img_path = img_path
        self.work_dir = work_dir
        self.img_scale = img_scale
        self.sigma = sigma
        self.kernel_size = kernel_size

    def make_kernel(self):
        kernel_1d = scipy.signal.windows.gaussian(2 * self.kernel_size + 1, self.sigma)
        kernel_2d = np.outer(kernel_1d, kernel_1d)
        self.kernel = kernel_2d / np.sum(kernel_2d)

    def convolve(self, img_channel):
        res = None
        for i in range(-self.kernel_size, self.kernel_size + 1):
            for j in range(-self.kernel_size, self.kernel_size + 1):
                tmp = (img_channel << i * self.original.shape[1] + j) * self.kernel[i + self.kernel_size, j + self.kernel_size]
                if res:
                    res += tmp
                else:
                    res = tmp
        return res

    def ceil_to_pow2(self, num):
        return 1 << int(np.ceil(np.log2(num)))

    def make_program(self):
        vec_size = self.ceil_to_pow2(self.img_size)
        program = eva.EvaProgram('Gaussian blur', vec_size)
        with program:
            for channel in self.channels:
                eva.Output(channel, self.convolve(eva.Input(channel)))
        program.set_input_scales(30)
        program.set_output_ranges(1)
        self.program = program

    def compile_program(self):
        compiler = CKKSCompiler(self.compiler_config)
        self.compiled, self.params, self.signature = compiler.compile(self.program)

    def make_keys(self):
        self.public_ctx, self.secret_ctx = generate_keys(self.params)

    def load_image(self):
        original = skimage.io.imread(self.img_path)
        original = skimage.util.img_as_float(original)
        self.original = skimage.transform.rescale(original, self.img_scale, channel_axis=-1)
        self.img_size = self.original.shape[0] * self.original.shape[1]

    def save_original(self):
        tmp = skimage.util.img_as_ubyte(self.original)
        skimage.io.imsave(f'{self.work_dir}/original.png', tmp)

    def make_inputs(self):
        inputs = {}
        for i, channel in enumerate(self.channels):
            cur_input = list(self.original[:, :, i].flat)
            cur_input.extend([0 for _ in range(self.signature.vec_size - self.img_size)])
            inputs[channel] = cur_input
        self.inputs = inputs

    def encrypt_inputs(self):
        self.enc_inputs = self.public_ctx.encrypt(self.inputs, self.signature)

    def save_inputs(self):
        eva.save(self.enc_inputs, f'{self.work_dir}/enc_inputs.bin')

    def save_state(self):
        for eva_field in self.eva_fields:
            eva.save(getattr(self, eva_field), f'{self.work_dir}/{eva_field}.bin')

    def load_outputs(self):
        self.enc_outputs = eva.load(f'{self.work_dir}/enc_outputs.bin')

    def decrypt_outputs(self):
        self.outputs = self.secret_ctx.decrypt(self.enc_outputs, self.signature)

    def recover_he_blured(self):
        img = np.empty_like(self.original)
        for i, channel in enumerate(self.channels):
            img[:, :, i].flat = self.outputs[channel][:self.img_size]
        self.he_blured = np.clip(img, 0, 1)

    def save_he_blured(self):
        tmp = skimage.util.img_as_ubyte(self.he_blured)
        skimage.io.imsave(f'{self.work_dir}/he_blured.png', tmp)

    def save_skimage_blured(self):
        tmp = skimage.util.img_as_ubyte(self.skimage_blured)
        skimage.io.imsave(f'{self.work_dir}/skimage_blured.png', tmp)

    def make_skimage_blured(self):
        self.skimage_blured = skimage.filters.gaussian(self.original, self.sigma, truncate=self.kernel_size/self.sigma, mode='wrap', channel_axis=-1)

    def save_skimage_mse(self):
        file = open(f'{self.work_dir}/skimage_mse.txt', 'w')
        file.write(f'MSE(original, skimage_blured): {np.mean((self.original - self.skimage_blured)**2)}\n')
        file.write(f'MSE(he_blured, skimage_blured): {np.mean((self.he_blured - self.skimage_blured)**2)}\n')
        file.close()

    def save_comparison_plot(self):
        plt.figure(figsize=(20, 10))
        plt.subplot(1, 3, 1)
        plt.title('Original image')
        plt.imshow(self.original)
        plt.subplot(1, 3, 2)
        plt.title('Homomorphic encryption gaussian blur')
        plt.imshow(self.he_blured)
        plt.subplot(1, 3, 3)
        plt.title('scikit-image gaussian blur')
        plt.imshow(self.skimage_blured)
        plt.savefig(f'{self.work_dir}/comparison_plot.png', bbox_inches='tight')

    def save_program(self):
        file = open(f'{self.work_dir}/program.dot', 'w')
        file.write(self.compiled.to_DOT())
        file.close()
        os.system(f'dot {self.work_dir}/program.dot -Tsvg -o {self.work_dir}/program.svg')

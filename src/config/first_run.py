def first_run(client, server):
    return [
        [client, 'load_image'],
        [client, 'make_kernel'],
        [client, 'make_program'],
        [client, 'compile_program'],
        [client, 'make_keys'],
        [client, 'make_inputs'],
        [client, 'encrypt_inputs'],
        [client, 'save_state'],
        [client, 'save_inputs'],

        [server, 'load_state'],
        [server, 'load_inputs'],
        [server, 'execute_program'],
        [server, 'save_outputs'],

        [client, 'load_outputs'],
        [client, 'decrypt_outputs'],
        [client, 'recover_he_blured'],
    ]

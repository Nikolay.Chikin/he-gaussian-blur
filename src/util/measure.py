import os
import resource
import time

import numpy as np


def run_once(config):
    res = []
    total_rel = 0
    total_sys_s = 0
    total_sys_u = 0
    for obj, method in config:
        rel_beg = time.time()
        sys_beg = resource.getrusage(resource.RUSAGE_SELF)
        getattr(obj, method)()
        rel_end = time.time()
        sys_end = resource.getrusage(resource.RUSAGE_SELF)
        rel = rel_end - rel_beg
        sys_s = sys_end.ru_stime - sys_beg.ru_stime
        sys_u = sys_end.ru_utime - sys_beg.ru_utime
        total_rel += rel
        total_sys_s += sys_s
        total_sys_u += sys_u
        name = f'{obj.name}.{method}()'
        res.append([name, rel, sys_s, sys_u])
    res.append(['Total time', total_rel, total_sys_s, total_sys_u])
    return res

def run_n_times(config, n):
    res = []
    runs = [run_once(config) for _ in range(n)]
    for f_res in zip(*runs):
        data = list(zip(*f_res))
        name = data[0][0]
        rel_m, rel_s = np.mean(data[1]), np.std(data[1])
        sys_s_m, sys_s_s = np.mean(data[2]), np.std(data[2])
        sys_u_m, sys_u_s = np.mean(data[3]), np.std(data[3])
        res.append([name, [rel_m, rel_s], [sys_s_m, sys_s_s], [sys_u_m, sys_u_s]])
    return res

def measure_time(config, n, path, prec=4):
    file = open(path, 'w')
    file.write(f'Running {n} times (mean ± std over {n} runs)\n\n')
    for name, rel, sys_s, sys_u in run_n_times(config, n):
        file.write(f'{name}:\n')
        file.write(f'\tReal time: {rel[0]:.{prec}f} ± {rel[1]:.{prec}f}\n')
        file.write(f'\tSystem time: {sys_s[0]:.{prec}f} ± {sys_s[1]:.{prec}f}\n')
        file.write(f'\tUser time: {sys_u[0]:.{prec}f} ± {sys_u[1]:.{prec}f}\n')
        file.write('\n')
    file.close()

def measure_artefact_sizes(config, work_dir):
    run_once(config)
    os.system(f'cd {work_dir} && du -bsh *.bin > artefact_sizes.txt')

def save_rusage(path):
    rusage = resource.getrusage(resource.RUSAGE_SELF)
    file = open(path, 'w')
    file.write(f'ru_maxrss = {rusage.ru_maxrss}\n')
    file.write(f'ru_minflt = {rusage.ru_minflt}\n')
    file.write(f'ru_majflt = {rusage.ru_majflt}\n')
    file.write(f'ru_inblock = {rusage.ru_inblock}\n')
    file.write(f'ru_oublock = {rusage.ru_oublock}\n')
    file.close()

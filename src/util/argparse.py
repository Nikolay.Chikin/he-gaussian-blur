import argparse

parser = argparse.ArgumentParser()

parser.add_argument('img_path', help='Path to source image')
parser.add_argument('work_dir', help='Path to the directory where to write the results')
parser.add_argument('number_of_threads', type=int, help='Number of threads')
parser.add_argument('number_of_runs', type=int, help='For how many runs to calculate the average and standard deviation execution time')
parser.add_argument('sigma', type=float, help='Gaussian blur sigma')
parser.add_argument('kernel_size', type=int, help='Gaussian blur kernel radius. Kernel is a square with side of 2 * kernel_size + 1')
parser.add_argument('img_scale', type=float, help='Float <= 1, scales the source image')

def save_args(args):
    file = open(f'{args.work_dir}/args.txt', 'w')
    file.write(f'{str(args)}\n')
    file.close()

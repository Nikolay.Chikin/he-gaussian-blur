FROM ubuntu:22.04

RUN apt-get update && apt-get install -y \
  git \
  cmake \
  libboost-all-dev \
  libprotobuf-dev \
  protobuf-compiler \
  clang \
  ninja-build \
  python3-pip \
  graphviz \
  && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install --no-cache-dir \
  numpy \
  scipy \
  scikit-image \
  matplotlib

RUN git clone --branch 4.1.1 https://github.com/microsoft/SEAL.git \
  && cd SEAL \
  && export CC=clang CXX=clang++ \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DSEAL_THROW_ON_TRANSPARENT_CIPHERTEXT=OFF . \
  && ninja install \
  && cd .. \
  && rm -rf SEAL

COPY eva.patch .

RUN git clone --recursive https://github.com/microsoft/EVA.git \
  && cd EVA \
  && git apply ../eva.patch \
  && export CC=clang CXX=clang++ \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DUSE_GALOIS=ON . \
  && ninja \
  && python3 -m pip install -e python \
  && python3 tests/all.py
